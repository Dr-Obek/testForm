package testform.mapper;

import testform.domain.ContactForm;
import testform.model.ContactFormRequestDto;
import testform.model.ContactFormResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ContactFormMapper {

    @Mapping(target = "kindOfRequest", ignore = true)
    ContactFormResponseDto mapToDto(ContactForm contactForm);

    @Mapping(target = "requestKind", ignore = true)
    ContactForm mapToDomain(ContactFormRequestDto contactFormRequestDto);
}
