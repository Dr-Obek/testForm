package testform.mapper;

import testform.domain.RequestKind;
import testform.model.RequestKindResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface RequestKindMapper {

    RequestKindResponseDto mapToDto(RequestKind requestKind);
}
