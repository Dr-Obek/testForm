package testform.api;

import org.springframework.security.access.annotation.Secured;
import testform.model.ContactFormRequestDto;
import testform.model.ContactFormResponseDto;
import testform.model.RequestKindResponseDto;
import testform.service.ContactFormService;
import testform.service.RequestKindService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * This controller class exposes REST API of the application
 */
@RestController
@RequestMapping("/contact")
@RequiredArgsConstructor
@Validated
public class FormController {

    private final ContactFormService contactFormService;
    private final RequestKindService requestKindService;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/request-kinds")
    public List<RequestKindResponseDto> getRequestKinds() {
        return requestKindService.getAllRequestKinds();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping
    public ContactFormResponseDto submitForm(@RequestBody @Valid final ContactFormRequestDto contactFormRequestDto) {
        return contactFormService.submitForm(contactFormRequestDto);
    }
}
