package testform.api;

import cz.ares.response.AresOdpovedi;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import testform.ws.AresClient;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ws")
public class TestWSController {

    private final AresClient aresClient;

    @Secured("ROLE_ADMIN")
    @GetMapping("{ico}")
    public AresOdpovedi getCompanyInfoByIco(@PathVariable("ico") String ico) {
        return aresClient.getCompanyInfo(ico);
    }
}
