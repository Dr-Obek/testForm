package testform.repository;

import testform.domain.RequestKind;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RequestKindRepository extends CrudRepository<RequestKind, Long> {

    Optional<RequestKind> findByKindOfRequest(String kindOfRequest);
}
