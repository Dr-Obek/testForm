package testform.ws;

import cz.ares.response.AresOdpovedi;

public interface AresClient {
    AresOdpovedi getCompanyInfo(String ico);
}
