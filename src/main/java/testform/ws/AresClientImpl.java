package testform.ws;

import cz.ares.request.AresDotazTyp;
import cz.ares.request.AresDotazy;
import cz.ares.request.Dotaz;
import cz.ares.response.AresOdpovedi;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class AresClientImpl extends WebServiceGatewaySupport implements AresClient {

    @Override
    public AresOdpovedi getCompanyInfo(String ico) {
        final AresDotazy aresDotazy = new AresDotazy();
        aresDotazy.setDotazPocet(1);
        aresDotazy.setDotazTyp(AresDotazTyp.VYPIS_RZP);
        final Dotaz dotaz = new Dotaz();
        dotaz.setICO(ico);
        dotaz.setPomocneID(1);
        aresDotazy.getDotaz().add(dotaz);

        return (AresOdpovedi) getWebServiceTemplate()
                .marshalSendAndReceive(aresDotazy);
    }
}