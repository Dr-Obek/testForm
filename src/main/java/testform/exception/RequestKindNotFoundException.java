package testform.exception;

import org.springframework.http.HttpStatus;

public class RequestKindNotFoundException extends MyException {

    public RequestKindNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
