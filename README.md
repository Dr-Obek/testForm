# Contact Form – Test Project
## Synopsis
This simple web application processes filling and submitting a contact form at http://localhost:8080. Permitted only for ADMIN and USER (for access see section Security below).<br/>
EDIT: The app also search for an entity in ARES (RŽP) by IČO at http://localhost:8080/ws/{ico}. ADMIN permitted only (for access see section Security below).
## API
OpenAPI available at http://localhost:8080/swagger-ui.html on application runtime. ADMIN permitted only (for access see section Security below).
## Install & run
The application can be build using ```mvn clean install```.
The built .jar file can be found in ~/target folder and run using ```java -jar testform-0.0.1-SNAPSHOT.jar```.
## Security – test entities
ADMIN: admin (adminPass)<br/>
USER: user1 (user1Pass)
## To be done
<ul>
<li>Connect to real database (PostgreSQL, ...)</li>
<li>Implement security (e.g. java.security, ...)</li>
<li>Dockerize the application</li>
<li>Improve Javadoc</li>
<li>Make FE dynamically defined (BE using Thymeleaf)</li>
<li>Better error handling on FE</li>
</ul>